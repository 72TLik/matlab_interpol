function newProp=interpOnTri(XY,xyAll,v,Props,MetOfInt,MetOfExt,MinMax)
%% Функция интерполяции с сетки с координатами xy, на сетку с координатами XY
met={'nearest','linear','natural','cubic','v4','krig','lapl'};
whos
for l=1:numel(v)
    if l~=1
        ind=sum(v(1:l-1))+[1:v(l)];
    else
        ind=1:v(l);
    end
    xy=xyAll(ind,:);
    prop=Props(ind,:);
    parfor i=1:size(Props,2)
        newProp(:,i)=razmaz(prop(:,i),met{MetOfInt(i)},XY,xy,MinMax(i,:));
    end
end
newProp=newProp';
newProp(isnan(newProp(:)))=0;
end

function B=razmaz(A,met,XY,xy,MinMax)
%for i=1:1000
switch met
    case 'krig'
        disp(met)
        B = krigingInterpolate(xy(isnan(A)==0,:),A(isnan(A)==0),XY(:,1),XY(:,2),5000);
    case 'lapl'
        disp(met)
        B=laplase_interpolant(xy(isnan(A)==0,:),A(isnan(A)==0),XY(:,1),XY(:,2));
    otherwise
        B=griddata(xy(isnan(A)==0,1), xy(isnan(A)==0,2), A(isnan(A)==0), XY(:,1), XY(:,2), met);
        B(B<MinMax(1))=MinMax(1);
        B(B>MinMax(2))=MinMax(2);
end
end