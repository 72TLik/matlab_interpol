xy=rand(5,2)*100;
a=(1:5)';

[X,Y]=meshgrid(0:100,0:100);
A=laplase_interpolant(xy,a,X(:),Y(:));

tri = delaunay([X(:);xy(:,1)],[Y(:);xy(:,2)]);
trisurf(tri,[X(:);xy(:,1)],[Y(:);xy(:,2)],[A;a],'FaceColor','interp','LineStyle','none')
hold on
plot3(xy(:,1),xy(:,2),a,'o')