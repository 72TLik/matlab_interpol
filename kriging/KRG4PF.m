function  Ph=KRG4PF(WD,Xp,Yp,vnk,licxy,wb,ilog)
global PR
wb.YData=0.05;      pause(0.001)
ilog(1,{'������������ �� ��������'})

WXY=WD.WXY;
Wi=WD.Wi;
wxy=xy4gor(WXY,Wi);

xy=licxy;%vnk{2}(2:end,:);
xy = interparc(100,xy(:,1),xy(:,2),'linear');

p=PR.Pcont*ones(size(xy,1),1);

Ph=zeros(size(Xp,1),size(Xp,2),size(WD.P,2),'single');
wb.YData=0.1;      pause(0.001)

for t=1:size(WD.P,2)
    P=double(WD.P(:,t));
    v=P~=0;

    [ Phat ] = krigingInterpolate( [wxy(v,:);xy], [P(v);p], Xp(:), Yp(:), 100);
    Ph(:,:,t)=reshape(Phat,size(Xp));
    wb.YData=0.1+0.9*t/size(WD.P,2);      pause(0.001)
end

wb.YData=1;
end

function par_krg
123
end
