function [ Zhat ] = krigingInterpolate( xy, z, X, Y, maxd)
maxdist = 1000;
k = 1;
while k
    
    v = variogram(xy,z,'plotit',false,'maxdist',maxdist);
    [~,~,~,vstruct] = variogramfit(v.distance,v.val,[],[],[],'model','spherical');
    
    if vstruct.range < maxdist
        break;
    end
    maxdist = 2*maxdist;
    if 0.5*maxd < maxdist
        break;
    end
    
end

[Zhat,Zvar] = kriging(vstruct,xy(:,1),xy(:,2),z,X,Y,size(X,1));

end

