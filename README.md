## Функция интерполяции с сетки с координатами xy, на сетку с координатами XY ##

## newProp=interpOnTri(XY,xyAll,v,Props,MetOfInt,MetOfExt,MinMax) ##

### Вход: ###
* XY - координаты сетки на которую происходит интерполяция
* xyAll - список координаты точек в которых известны значения какого-либо параметра
* v - массив размера n (количество интерполируемых параметров), каждый элемент которого указывает размер массива из xyAll и Props относящегося к i-тому параметру
* Props - значение параметра в каждой точке xyAll (вектор значений, определение индексов в этом векторе относящихся к тому или иному параметру определяется через v)
* MetOfInt - индекс метода интерполяции
* MetOfExt - индекс метода экстраполяции (не работает)
* MinMax - массив с ограничивающими значениями минимума и максимума для каждого параметра, n- строк, 2 столбца

met={'nearest','linear','natural','cubic','v4','krig','lap'};


### Выход ###
* newProp - массив интерполированных значений, каждая строка - отдельный параметр
вектор v используется так как в матлаб нельзя подавать массивы с разным кол-вом строк, приходилось преобразовывать в вектор