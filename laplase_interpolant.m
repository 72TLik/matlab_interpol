function B=laplase_interpolant(xy,a,X,Y) %gt,b1,A1,L,bg,n)
 
 XY=[X,Y]   ;
 XY=unique([xy;XY],'rows');
 n=size(XY,1);
  [~,ib]=ismember(xy,XY,'rows');
 
 licxy=XY(convhull(XY(:,1),XY(:,2)),:);
 bo=pointOrdet(licxy,XY);
Cp=[bo,[bo(2:end);bo(1)]];

 TR=delaunayTriangulation(XY,Cp);

IO = isInterior(TR);
FG=TR.ConnectivityList(IO,:);
TR1=triangulation(FG,XY(:,1),XY(:,2));

fe = freeBoundary(TR1);
 [B,sb,Sp]=Geome3(XY,TR1,fe);
[r,c]=find(B);
 L=((XY(r,1)-XY(c,1)).^2+(XY(r,2)-XY(c,2)).^2).^0.5;
L=sparse(r,c,1./L,n,n);

A1=B*L;
 b1=zeros(n,1);
    b1=b1+A1(:,ib)*a;
    %b1=b1+mean(gx.^2).^0.5*bg;
    v2=zeros(n,1);
    
    b1(ib)=[];
    v2(ib)=[];
    
    A1(ib,:)=[];
    A2=sparse(1:size(A1),1:size(A1),sum(A1,2)+v2);
    A1(:,ib)=[];
    
    Gpt=-b1'/(A1'-A2);
    
    %Gpt=bicgstab((A1'-A2),-b1,1e-3);
    
    GPT=zeros(n,1);%,'single'
    v1=1:n;
    v1(ib)=[];
    GPT(v1)=Gpt;
    GPT(ib)=a;
    
  [~,ib]=ismember([X,Y],XY,'rows');
  B=GPT(ib);
end